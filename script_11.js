// Завдання
// Написати реалізацію кнопки "Показати пароль".
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// У файлі index.html лежить розмітка двох полів вводу пароля.
// + Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, 
// які ввів користувач, іконка змінює свій зовнішній вигляд.
// + У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
// Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
// Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
// + Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
// Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – "You are welcome!";
// Якщо значення не збігаються - вивести під другим полем текст червоного кольору "Потрібно ввести однакові значення."
// Після натискання на кнопку сторінка не повинна перезавантажуватись
// Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.

let inputPassword = document.querySelectorAll('input[type="password"]');
// // let inputPassword = document.querySelectorAll(".password-form input");
console.log(inputPassword); // дістаємо всі input для паролів

// //Проміжний код: пробуємо змінити тип першого input, щоб при заповненні було видно символи
// // inputPassword[0].type = "text";
// // console.log(inputPassword);

// // Проміжний код: Перебираємо всі inputs і змінюємо їм атрибут type на text
// // inputPassword.forEach(function(input, index) {
// //     inputPassword[index].type = "text";
// // }); 

// дістаємо всі іконки з очком
let iconPassword = document.querySelectorAll('.icon-password');
console.log(iconPassword);

// // Перебираємо всі іконки з очком і вішаємо на них обробник на клік миші, а до нього функцію зміни типу input
iconPassword.forEach(function(icon, index) {
    icon.addEventListener("click", function() {
        icon.classList.toggle("fa-eye");
        icon.classList.toggle("fa-eye-slash");
        
        if (inputPassword[index].type === "password"){
            inputPassword[index].type = "text";
        }else{
            inputPassword[index].type = "password";
        }        
    })   
});

// Проміжний код: виводимо значення, що набрано в першому і в другому input окремо
// inputPassword[0].addEventListener("keyup", function() {console.log(inputPassword[0].value)});
// inputPassword[1].addEventListener("keyup", function() {console.log(inputPassword[1].value)});

//поєднуємо попередні два рядка в один код: виводимо значення, що набрано в inputах
inputPassword.forEach(function(input, index) {
    input.addEventListener("keyup", function() {console.log(inputPassword[index].value)});
});

// Змінну для червоного попередження оголошую і стилізую ззовні функції, щоб користуватись нею надалі
let newAlert = document.createElement("p");
newAlert.style.color = "red";
newAlert.style.margin = 0;
newAlert.style.position = "absolute";
newAlert.style.top = "150px";

const buttonSubmit = document.querySelector(".btn");
console.log(buttonSubmit);
buttonSubmit.before(newAlert);

// // Вішаємо на кнопку форми обробник по кліку миші
// buttonSubmit.addEventListener("click", function(){
//     if (inputPassword[0].value !== inputPassword[1].value) {            
//         newAlert.innerText = "Потрібно ввести однакові значення.";
//     } else if (inputPassword[0].value.trim() === "" && inputPassword[1].value.trim() === "") {
//         newAlert.innerText = "Поле обов'язкове до заповнення.";        
//     } else {
//         alert("You are welcome!");
//     }
// });

// // Переробила обробник - тепер слухає всю батьківську форму по події Submit
const form = document.querySelector(".password-form");
console.log(form);

form.addEventListener("submit", function(event){
    event.preventDefault();
    if (inputPassword[0].value !== inputPassword[1].value) {            
        newAlert.innerText = "Потрібно ввести однакові значення.";
    } else if (inputPassword[0].value.trim() === "" && inputPassword[1].value.trim() === "") {
        newAlert.innerText = "Поле обов'язкове до заповнення.";        
    } else {
        newAlert.innerText = "";
        // Щоб червоне попередження зникало до Alert, вкладемо Alert в setTimeout з відстрочкою в 0 мілісекунд
        setTimeout(function() {
            alert("You are welcome!");
        }, 0);

        // alert("You are welcome!");        
    }
});

// Додамо кнопку для очистки форми
let buttonReset = document.createElement ("button");
buttonReset.setAttribute("type", "reset");
buttonReset.classList.add("btn");
buttonReset.innerText = "Очистити форму";
buttonReset.style.margin = "24px 0 0 0"; 
// buttonReset.style.marginTop = "24px"; //(синтаксис запису, щоб записати margin-top)

buttonSubmit.after(buttonReset);
console.log(buttonReset);

//Хай червоне повідомлення очищується кнопкою "Очистити форму"
buttonReset.addEventListener("click", () => newAlert.innerText = "");

// // видалити повністю новостоврений <р> з червоним повідомленням - не вийшло
// // document.addEventListener("click", () => document.remove("newAlert"));
// // document.addEventListener("click", () => document.remove(newAlert));


// // console.log(inputPassword[0]); //  дістаємо перший label
// // console.log(inputPassword[0].children[0]); // дістаємо input в першому label
// // console.log(inputPassword[0].children[0].attributes); // дістаємо атрибут type в input в першому label
// // console.log(inputPassword[0].children[0].getAttribute("type")); // читаємо значення атрибут type в input в першому label


